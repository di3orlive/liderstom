// yandex maps
ymaps.ready(function(){

    var Places = [
        {
            coordinates:    [55.784962,37.613346],
            baloonHeader:   'Достоевская',
            baloonBody:     'ул. Октябрьская, дом 1',
            baloonFooter:   'Г. Москва <a href="record-step-4.html" class="balloon-link">Выбрать врача</a>'
        },
        {
            coordinates:    [55.843005,37.439633],
            baloonHeader:   'Сходненская',
            baloonBody:     'ул. Сходненская, дом 36/11',
            baloonFooter:   'Г. Москва <a href="record-step-4.html" class="balloon-link">Выбрать врача</a>'
        },
        {
            coordinates:    [55.655185,37.645092],
            baloonHeader:   'Каширская',
            baloonBody:     'Каширское шоссе, 24, стр. 2 (с торца здания)',
            baloonFooter:   'Г. Москва <a href="record-step-4.html" class="balloon-link">Выбрать врача</a>'
        },
        {
            coordinates:    [55.816462,37.507956],
            baloonHeader:   'Войковская',
            baloonBody:     'ул. Космонавта волкова, д. 5-1.',
            baloonFooter:   'Г. Москва <a href="record-step-4.html" class="balloon-link">Выбрать врача</a>'
        },
        {
            coordinates:    [55.738803,37.474682],
            baloonHeader:   'Филевский парк',
            baloonBody:     'ул. Малая филевская, 10, стр. 1',
            baloonFooter:   'Г. Москва <a href="record-step-4.html" class="balloon-link">Выбрать врача</a>'
        },
        {
            coordinates:    [55.714855,37.677150],
            baloonHeader:   'Дубровка',
            baloonBody:     'ул. Шарикоподшипниковская, дом 38 корп.3',
            baloonFooter:   'Г. Москва <a href="record-step-4.html" class="balloon-link">Выбрать врача</a>'
        }
    ];


    MAP = new ymaps.Map('yandexMap', {
        center:                     [55.76, 37.64],
        zoom:                       10
    });

    MAP.controls
        // Кнопка изменения масштаба.
        .add('zoomControl', { left: 5, top: 5 })
        // Список типов карты
        .add('typeSelector');

    var Marks = [];

    Places.forEach(function(P){
        var place = new ymaps.Placemark(P.coordinates, {
            balloonContentHeader:   P.baloonHeader,
            balloonContentBody:     P.baloonBody,
            balloonContentFooter:   P.baloonFooter,
            hintContent:            P.baloonHeader
        });
        Marks.push(place);
        MAP.geoObjects.add(place);


// var balloon = new ymaps.Balloon(MAP);
// balloon.options.setParent(MAP.options);
// balloon.open(MAP.getCenter());
    });



    MAP.options.set('scrollZoomSpeed', 5);

    $('.metroname li, .chooseClinic li').click(function () {
        $('.metroname li, .chooseClinic li').removeClass('active');
        var indx = $(this).index();
        $(this).addClass('active');
        MAP.panTo(Places[indx].coordinates,
            {
                flying: true,
                duration: 2500,
                delay: 500,
                callback: function () {
                    Marks[indx].balloon.open();
                    MAP.setZoom(17);
                }
            });
    });
});

var myMap;

// Дождёмся загрузки API и готовности DOM.
ymaps.ready(init);

function init () {
// Создание экземпляра карты и его привязка к контейнеру с
// заданным id ("map").
    myMap = new ymaps.Map('map', {
// При инициализации карты обязательно нужно указать
// её центр и коэффициент масштабирования.
        center:[55.76, 37.64], // Москва
        zoom:10
    });

    ymaps.controls.remove(trafficControl).remove('mapTools');

    document.getElementById('destroyButton').onclick = function () {
// Для уничтожения используется метод destroy.
        myMap.destroy();
    };
}











