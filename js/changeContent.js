$(document).ready(function() {

    var hash = window.location.hash.substr(1);
    var href = $('.pageChenge').each(function(){
        var href = $(this).attr('href');
        if(hash==href.substr(0,href.length-5)){
            var toLoad = hash+'.html #changeContent';
            $('#changeContent').load(toLoad)
        }
    });

    $('.pageChenge').click(function(e){

        var toLoad = $(this).attr('href')+' #changeContent';
        $('#changeContent').animate({ opacity: "0" }, 1000 ,loadContent );
        $('#load').remove();
        $('.wrapper').append('<span id="load">LOADING...</span>');
        $('#load').fadeIn('normal');
        window.location.hash = $(this).attr('href').substr(0,$(this).attr('href').length-5);
        function loadContent() {
            $('#changeContent').load(toLoad,'',showNewContent())
        }
        function showNewContent() {
            $('#changeContent').animate({ opacity: "1" }, 1000 ,hideLoader);
        }
        function hideLoader() {
            $('#load').fadeOut('normal');
        }
        return false;

    });

});