jQuery.exists = function (selector) { // функция проверки существования селектора
    return ($(selector).length > 0);
};

$(document).ready(function () {
    if($.exists('.fileupload')){
        $('.fileupload').on('change', function () {
            $('#fileinfo').text($(this).val());
        });
    }



    if($.exists('.icon')){
        $('.icon').on("click", function (e) {
            $('.submenu').toggle(
                function () {
                    $(this).animate({
                        top: '-152px'
                    }, 300);
                    $('.submenu').stop(true);
                },
                function () {
                    $(this).animate({
                        top: '76px'
                    }, 300);
                    $('.submenu').stop(true);
                });
            e.preventDefault();
        });
    }



    if($.exists('.drop-down')){
        $('.drop-down').click(function (e) {
            $(this).toggleClass('active');
            $(this).hasClass('active') ? $('ul', this).show() : $('ul', this).hide();

            if ($(e.target).closest('.drop-down').length < 1) {
                $('.drop-down').removeClass('active');
                $('.drop-down ul').hide();
            }

            e.preventDefault();
        });
    }


    if($.exists('.carousel-indicators li, .carousel-control')){
        $('.carousel-indicators li, .carousel-control').click(function () {
            setTimeout(function () {
                var left = $('.carousel-indicators li.active').offset().left;
                $('span.page-item').css({left: left + 'px'});
            }, 50);
        });
        $('.carousel-indicators li.active').click();
        $(window).resize(function () {
            $('.carousel-indicators li.active').click();
        });
    }



    if($.exists('#getLoginForm')){
        function toggleLoginForm() {
            $('#getLoginForm').hasClass('active') ?
                $('.login-form').css({marginTop: 40, opacity: 0})
                    .show()
                    .animate({marginTop: 10, opacity: 1}, 200) :
                $('.login-form').fadeOut(300)
        }

        $('#getLoginForm').click(function (e) {
            if ($(e.target).closest('.login-form, .success-form').length == 0) {
                $(this).toggleClass('active');
                toggleLoginForm();
            }
            e.preventDefault();
        });
        $('body').click(function (e) {
            if ($(e.target).closest('#getLoginForm').length == 0) {
                $('#getLoginForm').removeClass('active');
                toggleLoginForm();
                $('.success-form').fadeOut(200);
            }
        });
        $('.login-form button').click(function (e) {
            $('#getLoginForm').removeClass('active');
            toggleLoginForm();
            setTimeout(function () {
                $('.success-form')
                    .css({marginTop: 40, opacity: 0}).show()
                    .animate({marginTop: 10, opacity: 1}, 200);
            }, 300);
            e.preventDefault();
        });
        $('.close-loginform').click(function (e) {
            $('#getLoginForm').removeClass('active');
            toggleLoginForm();
            $('.success-form').fadeOut(200);
            e.preventDefault();
        });
    }



    if($.exists('textarea')){
        $('textarea').autoResize();
    }


    if($.exists('.phone-mask')){
        $(".phone-mask").mask("+7(999) 999-99-99", {placeholder: "+7(___) ___-__-__"});
    }



    if($.exists('.form')){
        $('.form').on('submit', function (event) {
            event.preventDefault();

            var form = $(this);
            var validate_error = false;
            var user_name = form.find('#user_name, .user_name');
            var email_validate = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
            var phone_validate = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
            var user_email = form.find('#user_email, .user_email');
            var email_val = user_email.val();
            var user_phone = form.find('#user_phone, .user_phone');
            var phone_val = user_phone.val();

            if (user_name.val().length < 2) {
                user_name.addClass('validate-error');
                user_name.parent().addClass('error-message');
                validate_error = true;
            }

            if (email_val.length < 2) {
                user_email.addClass('validate-error');
                user_email.parent().addClass('error-message').css('content','ass');
                validate_error = true;
            } else if (!email_validate.test(email_val)) {
                user_email.addClass('validate-error');
                user_email.parent().addClass('error-message').css('content','hole');
                validate_error = true;
            }

            if (user_phone.val().length < 2) {
                user_phone.addClass('validate-error');
                user_phone.parent().addClass('error-message');
                validate_error = true;
            }


//        if (!phone_validate.test(phone_val)) {
//            user_email.addClass('validate-error');
//            validate_error = true;
//        }

        });
    }



    if($.exists('.next-window')){
        $('.next-window').click(function (e) {
            e.preventDefault();
            $('.hide-box').animate({ height: '100%' }, 800);
            $('.header').animate({ top: ($(window).height() - $('.header').height()) }, 800);
            $('.wrapper').slideUp(800);

            var thisWindow = $(this).attr('href');

            setTimeout(function () {
                window.location.replace(thisWindow);
            }, 800);
        });
    }



//    if($.exists('.masterTooltip')){
//        $('.masterTooltip').hover(function () {
//            // Hover over code
//            var title = $(this).attr('title');
//            $(this).data('tipText', title).removeAttr('title');
//            $('<p class="tooltip"></p>')
//                .text(title)
//                .appendTo('body')
//                .fadeIn('slow');
//        }, function () {
//            // Hover out code
//            $(this).attr('title', $(this).data('tipText'));
//            $('.tooltip').remove();
//        }).mousemove(function (e) {
//            var mousex = e.pageX - 103; //Get X coordinates
//            var mousey = e.pageY + 30; //Get Y coordinates
//            $('.tooltip')
//                .css({ top: mousey, left: mousex })
//        });
//    }



    if($.exists('.openMore')){
        $('.openMore').click(function () {

            $.fn.extend({
                    toggleText: function (a, b) {
                        if (this.text() == a) {
                            this.text(b);
                        }
                        else {
                            this.text(a)
                        }
                    }}
            );
            $(this).toggleText("Показать все услуги", "Скрыть все услуги");

            $('#service-hide').stop(true).toggle('slow');
        });
    }



    if($.exists('#fixed-box')){
        var asideX = document.querySelector('#fixed-box'),
            t0X = asideX.getBoundingClientRect().top - document.documentElement.getBoundingClientRect().top - 90,
            t1X = document.documentElement.scrollHeight - 890 - asideX.offsetHeight;

        function asideScrollX() {
            if (window.pageYOffset > t1X) {
                asideX.className = (window.pageYOffset > t1X ? 'stop' : 'service-siteBar');
                asideX.style.top = t1X - t0X + 'px';
            } else {
                asideX.className = (t0X < window.pageYOffset ? 'prilip' : 'service-siteBar');
                asideX.style.top = '90px';
            }
        }

        window.addEventListener('scroll', asideScrollX, false);
    }



    if($.exists('#fixed-box2')){
        var aside = document.querySelector('#fixed-box2'),
            t0 = aside.getBoundingClientRect().top - document.documentElement.getBoundingClientRect().top - 90,
            t1 = document.documentElement.scrollHeight - 740 - aside.offsetHeight;

        function asideScroll() {
            if (window.pageYOffset > t1) {
                aside.className = 'stop';
                aside.style.top = t1 - t0 + 'px';
            } else {
                aside.className = (t0 < window.pageYOffset ? 'prilip' : '');
                aside.style.top = '90px';
            }
        }

        window.addEventListener('scroll', asideScroll, false);
    }




    if($.exists('.accordion-link')){
        $('.accordion-link').click(function () {
            $('.accordion-link').each(function () {
                $(this).find('span').removeClass('svg-liast-arrow-b-close');
            });
            if ($(this).hasClass('collapsed')) {
                $(this).find('span').addClass('svg-liast-arrow-b-close');
            } else {
                $(this).find('span').removeClass('svg-liast-arrow-b-close');
            }
        });
    }



    if($.exists('.back2records')){
        $('.back2records').hide();
        var y = /([A-Za-z0-9-]+.html)$/;
        var z = document.referrer.match(y);
        if (z[0] === 'record-pic-2.html' || z[0] === 'record-step-5.html') {
            $('.back2records').slideDown();
            $('.back2records').find('a').click(function () {
                window.history.back();
            })
        }
    }



    if($.exists('#container')){
        var $container = $('#container');
        $container.packery({
            itemSelector: '.doc-item',
            gutter: 0
        });

    }



    if($.exists('#container2')){
        var $container2 = $('#container2');
        $container2.packery({
            itemSelector: '.item',
            gutter: 0
        });
    }



    if($.exists('.vote-result')){
        $('.vote-result').hide();
        $('.vote-link').click(function (e) {
            e.preventDefault();
            $('.vote-ques').toggle().hide();
            $('.vote-result').toggle().show();
        });
    }



    if($.exists('#sticker')){
        topBlockHeightMain = $('#sticker').height();
        $(window).scroll(function () {
            // var blockOtzuv = $('.blockotzuv').
            var topScroll = $(document).scrollTop();
            topBlock = $('[data-fixedblock-anchor=""]').offset().top - 70;
            topBlockHeight = $('#sticker').height();
            topBlockBraker = $('[data-fixedblock-braker=""]').offset().top - 70;
            if (topScroll + topBlockHeight + 90 > topBlockBraker) {
                $('#sticker')
                    .css({height: topBlockHeightMain + 'px !important'})
                    .removeClass('fixed-top').addClass('abs-bot');
            } else if (topScroll > topBlock) {
                $('#sticker').removeClass('abs-bot').addClass('fixed-top');
            } else if (topScroll < topBlock) {
                $('#sticker').removeClass('abs-bot fixed-top');
            }
        });
    }



    if($.exists('#source')){
        $('#source').quicksand( $('#destination li') );
    }



    if($.exists('.scroll-link')){
        $('a[href^="#"].scroll-link').on('click',function (e) {
            e.preventDefault();

            var target = this.hash;
            var $target = $(target);

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 900, 'swing', function () {
                window.location.hash = target;
            });
        });
    }



    if($.exists('.reset-btn')){
        $('.reset-btn').click(function () {
            $(this).parent().parent().parent().parent().find('.servicelnk').click();

        });
    }




    if($.exists('.tooltipX')){
        $('.tooltipX').tipso({
            background  : '#494949',
            color: '#ffffff',
            position: 'bottom',
            width: 260,
            speed: 10
        });

    }



//    if($.exists('.slider')){
//        $(".slider").swiperight(function() {
//            $(this).carousel('prev');
//        });
//        $(".slider").swipeleft(function() {
//            $(this).carousel('next');
//        });
//    }



    if($.exists('#filter')){
        $('#filter > li').on('click', function () {
            var filter = $(this).data('filter');
            var doctorId = $('#doctors > li');
            var fade = 500;

            doctorId.hide();
            doctorId.each(function () {
                var id = $(this).data('doctorId');
                if(filter == id){
                    $(this).fadeIn(fade);
                    fade = fade + 500;
                }else if(filter == 0){
                    $(this).fadeIn(fade);
                }
            });

            $('.doctors-link').click();
        });
    }



    if($.exists('.scroll-link')){
//        $('.scroll-link').click(function (e) {
//            e.preventDefault();
//            var target = $('.scroll-to-link');
//            $(this).animate({
//                scrollTop: target.offset().top
//            }, 1000);
//        })
    }












    $('body').on('focus', '.validate-error', function () {
        $(this).removeClass('validate-error');
        $(this).parent().removeClass('error-message');
    });



    $(document).mouseup(function (e){
        var div = $(".submenu");
        var div2 = $(".header");
        var div3 = $("#filter");
        if (div.has(e.target).length === 0 && div2.has(e.target).length === 0) {
            $('.submenu').slideUp();
        }
//        if (div3.has(e.target).length === 0) {
//            $('.choose-doctor').slideUp();
//        }

        $(this).scroll(function () {
            $('.submenu').slideUp();
//            $('.choose-doctor').slideUp();
            $('#service-hide').hide();
            $('.openMore').text("Показать все услуги");
        });
    });
});