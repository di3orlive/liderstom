jQuery.exists = function (selector) { // функция проверки существования селектора
    return ($(selector).length > 0);
};

if($.exists('.progress-pie-chart')){

}







$(document).ready(function() {
    if($.exists('.icon')){
        $('.icon').on("click",function(e){
            $('.submenu').toggle(
                function () {
                    $(this).animate({
                        bottom: '-156px'
                    }, 300 );
                },
                function () {
                    $(this).animate({
                        bottom: '79px'
                    }, 300 );
                });
            e.preventDefault();
        });
    }



    if($.exists('.shtorka ul li:not(:first-child)')){
        $('.shtorka ul li:not(:first-child)').hover(function(){
            $('.shtorka ul li:not(:first-child)').removeClass("effect");
            $(this).addClass("effect");
        });
    }



    if($.exists('.load-more-history')){
        var action = 1;
        var $elem = $('html');
        function viewSomething() {
            if (action == 1) {
                $(".history-box:first").clone().css({opacity:'0', left:'-200px'}).animate({
                    opacity: 1,
                    left: "0"
                }, 500).appendTo(".history-box-l");
                action = 2;
            } else {
                $(".history-box-r .history-box:first").clone().css({marginTop:'20px', opacity:'0', right:'-200px'}).animate({
                    opacity: 1,
                    right: '0'
                }, 500).appendTo(".history-box-r");
                action = 1;
            }

            var target = $( $('.history-link a').attr('href') );
            if( target.length ) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
            }
        }
        $(".load-more-history").on("click", viewSomething);
    }



    if($.exists('.map')){
        $('.map').maphilight({strokeColor: '379ee0', fillColor: '379ee0', fillOpacity: 0.6});

        var data = $(this).data('maphilight') || {};
        data.alwaysOn = true;
        $('#map1').data('maphilight', data).trigger('alwaysOn.maphilight');
    }



    if($.exists('.tab-content')){
        $(".tabs area:eq(0)").each(function () {
            $(this).addClass("current");
        });
        $(".tab-content").each(function () {
            $(this).children(":not(:first)").hide();
        });


        $(".tabs area").click(function (e) {
            e.preventDefault();

            $(".tabs area").each(function () {
                var d = $(this).data('maphilight') || {};
                if (d.alwaysOn == true) {
                    d.alwaysOn = false;
                }
            });

            var data = $(this).data('maphilight') || {};
            data.alwaysOn = true;
            $(this).data('maphilight', data).trigger('alwaysOn.maphilight');

            if ($(this).hasClass("current") == false) {
                var thisTarget = $(this).attr("href");
                $(this).parents(".tabs").find('area.current').removeClass('current');
                $(this).addClass('current');
                $(this).parents(".tabs").nextAll(".tab-content").children(":visible").fadeOut(1, function () {
                    $(thisTarget).fadeIn("fast");
                });
            }
            return false;
        });
    }



    if($.exists('.open-map')){
        $('.open-map').click(function () {
            if ($(this).hasClass('collapsed')) {
                $(this).find('span').addClass('cabinet-box3-inside-close');
            } else {
                $(this).find('span').removeClass('cabinet-box3-inside-close');
            }
        });
    }



    if($.exists('.chooseClinic')){
        $('#next-step').addClass('not-active-link');

        $('.chooseClinic ul li').click(function () {
            var chooseClinic = $(this).find('a').text();
            $('.open-map').find('div').text(chooseClinic).click();

            $('#next-step').removeClass('not-active-link');
        });
    }



    if($.exists('.datepicker')){
        $('.datepicker').datepicker();
    }



    if($.exists('.rec-dot')){

        var y = /([A-Za-z0-9-]+.html)$/;
        var z = window.location.href.match(y);

        if(z[0]  == 'record-step-2.html' || z[0]  == 'record-pic-1.html' || z[0]  == 'record-pic-2.html'){
            $('.rec-line .rec-dot:gt(0)').css({
                pointerEvents: 'none'
            })
        } else if(z[0]  == 'record-step-3.html' || z[0]  == 'record-pic-3.html'){
            $('.rec-line .rec-dot:gt(1)').css({
                pointerEvents: 'none'
            })
        } else if(z[0]  == 'record-step-4.html'){
            $('.rec-line .rec-dot:gt(2)').css({
                pointerEvents: 'none'
            })
        } else if(z[0]  == 'record-step-5.html'){
            $('.rec-line .rec-dot:gt(2)').css({
                pointerEvents: 'none'
            })
        }
    }
























    if($.exists('textarea')){
        $('textarea').autoResize();
    }



    if($.exists('.phone-mask')){
        $(".phone-mask").mask("+7(999) 999-99-99", {placeholder: "+7(___) ___-__-__"});
    }



    $('body').on('focus', '.validate-error', function () {
        $(this).removeClass('validate-error');
        $(this).parent().removeClass('error-message');
    });



    if($.exists('#getLoginForm')){
        function toggleLoginForm() {
            $('#getLoginForm').hasClass('active') ?
                $('.login-form').css({marginTop: 40, opacity: 0})
                    .show()
                    .animate({marginTop: 10, opacity: 1}, 200) :
                $('.login-form').fadeOut(300)
        }

        $('#getLoginForm').click(function (e) {
            if ($(e.target).closest('.login-form, .success-form').length == 0) {
                $(this).toggleClass('active');
                toggleLoginForm();
            }
            e.preventDefault();
        });
        $('body').click(function (e) {
            if ($(e.target).closest('#getLoginForm').length == 0) {
                $('#getLoginForm').removeClass('active');
                toggleLoginForm();
                $('.success-form').fadeOut(200);
            }
        });
        $('.login-form button').click(function (e) {
            $('#getLoginForm').removeClass('active');
            toggleLoginForm();
            setTimeout(function () {
                $('.success-form')
                    .css({marginTop: 40, opacity: 0}).show()
                    .animate({marginTop: 10, opacity: 1}, 200);
            }, 300);
            e.preventDefault();
        });
        $('.close-loginform').click(function (e) {
            $('#getLoginForm').removeClass('active');
            toggleLoginForm();
            $('.success-form').fadeOut(200);
            e.preventDefault();
        });
    }



    if($.exists('.form')){
        $('.form').on('submit', function (event) {
            event.preventDefault();

            var form = $(this);
            var validate_error = false;
            var user_name = form.find('#user_name, .user_name');
            var email_validate = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
            var phone_validate = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
            var user_email = form.find('#user_email, .user_email');
            var email_val = user_email.val();
            var user_phone = form.find('#user_phone, .user_phone');
            var phone_val = user_phone.val();

            if (user_name.val().length < 2) {
                user_name.addClass('validate-error');
                user_name.parent().addClass('error-message');
                validate_error = true;
            }

            if (email_val.length < 2) {
                user_email.addClass('validate-error');
                user_email.parent().addClass('error-message').css('content','ass');
                validate_error = true;
            } else if (!email_validate.test(email_val)) {
                user_email.addClass('validate-error');
                user_email.parent().addClass('error-message').css('content','hole');
                validate_error = true;
            }

            if (user_phone.val().length < 2) {
                user_phone.addClass('validate-error');
                user_phone.parent().addClass('error-message');
                validate_error = true;
            }


//        if (!phone_validate.test(phone_val)) {
//            user_email.addClass('validate-error');
//            validate_error = true;
//        }

        });
    }
});